# Initialize server project

1. Go through this [guide](https://developers.amadeus.com/self-service/category/air/api-doc/airport-and-city-search/api-reference) to get your clientId and clientSecret
1. `mkdir server && cd server && npm init -y`
2. `npm i amadeus axios express`
3. `mkdir src && cd src`
4. ```js
   // src/config
   const dotenv = require("dotenv");
   dotenv.config();

   // Exporting env variable
   module.exports = {
     CLIENT_ID: process.env.CLIENT_ID,
     CLIENT_SECRET: process.env.CLIENT_SECRET
   };
   ```

5. ```js
    // src/index.js
    const express = require("express");
    const router = require("./router");
    const path = require("path")

    const PORT = 1338;

    const app = express();

    // applying handler for API
    app.use("/", router);

    // Handling serving static files
    app.use(express.static(path.join(__dirname, '../client/build')));

    // Serving app on PORT we defined
    app.listen(PORT, () => {
      console.log(`Express is running on port ${PORT}`);
    });

   ```

6.  ```js
    // src/router.js
    const Amadeus = require("amadeus");
    const router = require("express").Router();

    // Getting env variables 
    const { CLIENT_ID, CLIENT_SECRET } = require('./config');

    const API = `api`;

    // This is AMADEUS client for getting authToken that we need to make actual call to amadeus API 
    const amadeus = new Amadeus({
      clientId: CLIENT_ID,
      clientSecret: CLIENT_SECRET
    });

    // Endpoint
    router.get(`/${API}/airports`, async (req, res) => {
      const { page, subType, keyword } = req.query;

      // API call with params we requested from client app
      const response = await amadeus.client.get("/v1/reference-data/locations", {
        keyword,
        subType,
        "page[offset]": page * 10
      });

      // Sending response for client
      try {
        await res.json(JSON.parse(response.body));
      } catch (err) {
        await res.json(err);
      }
    });

    module.exports = router;

     ```
7. `cd ../ && touch .env`
8.  ```env
    CLIENT_ID={your clientId}
    CLIENT_SECRET={your clientSecret}
    ```
9.  Add start scripts in you package.json
    ```json
    // package.json
    ...
    "scripts": {
      "start": "node src/index.js",
      "build": "cd ../client && npm run build && cd ../server"
    },
    ...
    ```
    And run `npm start`


# Initialize client project
1. Open new terminal in `my-app` directory
2. `npm i -g create-react-app`
2. `create-react-app client`
3. `cd client`
4. `npm i @material-ui/core @material-ui/icons @material-ui/lab axios node-sass query-string react-router-dom`
5. `mkdir api components images pages styles`

## Replace files with these:
1.  ```jsx
    // index.js 
    import React from 'react';
    import ReactDOM from 'react-dom';
    import App from './App';

    // Rendering our app to the DOM
    ReactDOM.render(<App />, document.getElementById('root'));
    ```

2.  ```jsx
    // App.js 
    import React from "react";
    import SearchRoot from "./pages/search-root";
    import "./styles/main.scss";
    // import * as logo from './images/logo-portal.png'

    // Root component
    const App = () => {
      return (
        <>
           {/*<div className="logo">
            <img src={logo} alt="logo" draggable={false} />
          </div>*/}
          <SearchRoot />
        </>
      );
    };

    export default App;
    ```

3.  */api*
    ```jsx
    // api/amadeus.api.js

    import axios from "axios";

    const CancelToken = axios.CancelToken;

    // This function allow you to make GET request to backend with params we need
    export const getAmadeusData = params => {

      // Destructuring params
      const { keyword = "", page = 0, city = true, airport = true } = params;

      // Checking for proper subType 
      const subTypeCheck = city && airport ? "CITY,AIRPORT" : city ? "CITY" : airport ? "AIRPORT" : ""

      // Amadeus API require at least 1 character, so with this we can be sure that we can make this request
      const searchQuery = keyword ? keyword : "a";

      // This is extra tool for cancelation request, to avoid overload API 
      const source = CancelToken.source();

      // GET request with all params we need
      const out = axios.get(
        `/api/airports/?keyword=${searchQuery}&page=${page}&subType=${subTypeCheck}`,
        {
          cancelToken: source.token
        }
      )

      return { out, source }
    };
    ```

4. `touch pages/search-root.js`

    ```jsx
    // pages/search-root.js
    import React from "react";
    import SearchTable from "../components/search-table";
    import SearchAutocomplete from "../components/search-autocomplete";
    import { getAmadeusData } from "../api/amadeus.api";
    import axios from "axios"
    import SearchCheckboxes from "../components/search-checkboxes";

    // Main component 
    const SearchRoot = () => {

      /* 
        With new React API we can define state in functional component like this *React.useState*
        1. first element in desctructured array - state itself
        2. second element - dispatch func, that allows us to change state
        3. we can create as many states as we need
      */
      const [search, setSearch] = React.useState({
        keyword: "a",
        city: true,
        airport: true,
        page: 0
      });

      const [dataSource, setDataSource] = React.useState({
        meta: { count: 0 },
        data: []
      });

      const [loading, setLoading] = React.useState(false)

      /* 
        Also React has lifecycle methods. On of them is *useEffect* - the same as ComponentDidMount | ComponentDidUpdate | ComponentWillUnmount in class components 
        1. First argument is callback func, we define there all logic we need to execute when component mounts
        2. Second argument - array of dependencies. If one of them changing - we executing callback func again

        ** If Array is empty - callback will execute only once, when mount
        ** If you not including second argument - callback will execute each time, when component will change

        3. We can create as many *useEffect* funcs, as we need
      */
      React.useEffect(() => {
        // Turn on loader animation
        setLoading(true)


        /* Getting data from amadeus api.
          out - our data that coming from backend.
          source - token for cancelation request.
        */

        const { out, source } = getAmadeusData(search);

        out.then(res => {
          // If we send too many request to the api per second - we will get an error and app will break
          // Therefore we implemented simple check to prevent error on client side.
          if (!res.data.code) {
            setDataSource(res.data); // dispatching data to components state
          }
          setLoading(false)
        }).catch(err => {
          axios.isCancel(err);
          setLoading(false)
        });

        // If we returning function from *useEffect* - then this func will execute, when component will unmount
        return () => {
          source.cancel()
        };
      }, [search]);

      return (
        <div className="container">
          <div className="search-panel">
            <SearchAutocomplete search={search} setSearch={setSearch} />
            <SearchCheckboxes search={search} setSearch={setSearch} />
          </div>
          <SearchTable dataSource={dataSource} search={search} setSearch={setSearch} loading={loading} />
        </div>
      );
    };

    export default SearchRoot;


    ```

5. `cd components && touch search-autocomplete.js search-checkboxes.js search-table.js && cd../`
    ```jsx
    // search-autocomplete.js
    import React, { useCallback, useEffect } from "react";
    import Autocomplete from "@material-ui/lab/Autocomplete";
    import TextField from "@material-ui/core/TextField";
    import CircularProgress from "@material-ui/core/CircularProgress";
    import axios from 'axios'
    import { getAmadeusData } from "../api/amadeus.api";
    import { debounce } from "lodash"

    const SearchAutocomplete = (props) => {
      const [open, setOpen] = React.useState(false);
      const [options, setOptions] = React.useState([]);
      const [search, setSearch] = React.useState('')
      const [keyword, setKeyword] = React.useState('')
      const [loading, setLoading] = React.useState(false)

      // Configure options format for proper displaying on the UI
      const names = options.map(i => ({ type: i.subType, name: i.name }));

      // Debounce func prevents extra unwanted keystrokes, when user triggers input events  
      const debounceLoadData = useCallback(debounce(setKeyword, 1000), []);

      useEffect(() => {
        debounceLoadData(search);
      }, [search]);

      // Same example as in *SearchRoot* component
      React.useEffect(() => {

        setLoading(true)
        const { out, source } = getAmadeusData({ ...props.search, page: 0, keyword });

        out.then(res => {
          if (!res.data.code) {
            setOptions(res.data.data);
          }
          setLoading(false)
        }).catch(err => {
          axios.isCancel(err);
          setOptions([]);
          setLoading(false)

        });

        return () => {
          source.cancel()
        };
      }, [keyword]);

      // Desctructuring our props
      const { city, airport } = props.search

      const label = city && airport ? "City and Airports" : city ? "City" : airport ? "Airports" : ""

      return (
        // This is Material-UI component that also has it's own props
        <>
          <Autocomplete
            id="asynchronous-demo"
            style={{ width: 300, marginBottom: "1rem" }}
            open={open}
            onOpen={() => {
              setOpen(true);
            }}
            onClose={() => {
              setOpen(false);
            }}
            getOptionSelected={(option, value) =>
              option.name === value.name && option.type === value.type
            }
            onChange={(e, value) => {
              if (value && value.name) {
                props.setSearch((p) => ({ ...p, keyword: value.name, page: 0 }))
                setSearch(value.name)
                return;
              }
              setSearch("")
              props.setSearch((p) => ({ ...p, keyword: "a", page: 0 }))

            }}
            getOptionLabel={option => {
              return option.name;
            }}
            options={names}
            loading={loading}
            renderInput={params => {
              return (
                <TextField
                  label={label}
                  fullWidth
                  onChange={e => {
                    e.preventDefault();
                    setSearch(e.target.value);
                  }}
                  variant="outlined"
                  inputProps={{
                    ...params.inputProps,
                    value: search
                  }}
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {loading ? (
                          <CircularProgress color="inherit" size={20} />
                        ) : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    )
                  }}
                />
              );
            }}
          />
        </>
      )
    };

    export default SearchAutocomplete;

    ```

    ```jsx
    // search-checkboxes.js
    import React from 'react'
    import { FormControlLabel, Checkbox } from '@material-ui/core'

    const SearchCheckboxes = (props) => {
      const { city, airport } = props.search

      // Handle change event on clicking checkboxes
      const onCheckboxChange = (e) => {
        e.persist();
        if (e.target.checked && (city || airport)) {
          props.setSearch(p => ({ ...p, [e.target.value]: e.target.checked }));
          return;
        }
        if (!e.target.checked && !(!city || !airport)) {
          props.setSearch(p => ({ ...p, [e.target.value]: e.target.checked }));
          return;
        }
      };


      return (
        <div>
          <FormControlLabel
            control={
              <Checkbox
                checked={city}
                onChange={onCheckboxChange}
                value={"city"}
              />
            }
            label="City"
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={airport}
                onChange={onCheckboxChange}
                value={"airport"}
              />
            }
            label="Airport"
          />
        </div>
      )
    }

    export default SearchCheckboxes
    ```

    ```jsx
    // search-table.js
    import React from "react";
    import { makeStyles } from "@material-ui/core/styles";
    import Table from "@material-ui/core/Table";
    import TableBody from "@material-ui/core/TableBody";
    import TableCell from "@material-ui/core/TableCell";
    import TableHead from "@material-ui/core/TableHead";
    import TablePagination from "@material-ui/core/TablePagination";
    import TableRow from "@material-ui/core/TableRow";
    import Paper from "@material-ui/core/Paper";
    import LinearProgress from '@material-ui/core/LinearProgress';


    const useStyles = makeStyles({
      root: {
        width: "100%",
        overflowX: "auto"
      },
      table: {
        minWidth: 650
      }
    });

    const SearchTable = props => {
      const { data, meta } = props.dataSource;
      const classes = useStyles();

      return (

        <Paper className={classes.root} style={{ position: "relative" }}>

          {/* Loader for improving UX */}
          {props.loading && <LinearProgress variant="indeterminate" className="linear-progress" />}

          <Table className={classes.table} aria-label="simple table">

            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell align="right">Type</TableCell>
                <TableCell align="right">City</TableCell>
                <TableCell align="right">Country</TableCell>
                <TableCell align="right">Country code</TableCell>
              </TableRow>
            </TableHead>

            <TableBody >
              {!!data.length &&
                data.map((row, idx) => (
                  <TableRow key={row.name + idx}>
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="right">{row.subType}</TableCell>
                    <TableCell align="right">{row.address.cityName}</TableCell>
                    <TableCell align="right">{row.address.countryName}</TableCell>
                    <TableCell align="right">{row.address.countryCode}</TableCell>
                  </TableRow>
                ))}
            </TableBody>

          </Table>

          {/* Will display this if no data in response from server */}
          {!data.length && <div className="center">No data</div>}

          {/* Pagination Component */}
          <TablePagination
            rowsPerPageOptions={[]}
            component="div"
            count={meta.count ? meta.count : 0}
            rowsPerPage={10}
            page={props.search.page}
            onChangePage={(e, page) => {
              props.setSearch(p => ({ ...p, page }));
            }}
          />
        </Paper>
      );
    };

    export default SearchTable;
    ```

6. `touch styles/main.scss`

    ```scss
    // styles/main.scss
    .container {
      max-width: 1368px;
      margin: 0 auto;
      padding: 1rem;
    }

    .center {
      width: 100%;
      padding: 2rem 0;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .search-panel { 
      display: flex;
      justify-content: space-between;
    }

    .linear-progress {
      position: absolute !important;
      width: 100% !important;
    }

    .logo {
      padding: 1rem 0;
      margin-bottom: 1rem;
      display: flex;
      justify-content: center;
      user-select: none;
      border-bottom: 1px solid #c6c6c6;
      img {
        max-width: 500px;
        width: 100%;
      }
    }
    ```

7.  in `package.json` add this line to create proxy with our backend
    ```json
    ...
    
    "proxy": "http://localhost:1338",

    ...
    ```
8. run `npm start`